package exceptions;

@SuppressWarnings("serial")
/**
 * Exception qui est trigger quand on essaye d'intérragir avec une place déjà occupée
 * @author qbern
 *
 */
public class OccupiedPlaceException extends CustomException{

	public OccupiedPlaceException() {
		super("Occupied place", "This place is not free.");
	}

}
