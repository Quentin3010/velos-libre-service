package exceptions;

@SuppressWarnings("serial")
/**
 * Exception qui est trigger lorsqu'on essaye d'intérragir avec une place déjà vide
 * @author qbern
 *
 */
public class AlreadyFreePlaceException extends CustomException{

	public AlreadyFreePlaceException() {
		super("Already Free Place", "This place is already free.");
	}

}
