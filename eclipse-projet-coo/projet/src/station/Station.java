package station;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import centre.Centre;
import exceptions.AlreadyFreePlaceException;
import exceptions.OccupiedPlaceException;
import others.ColorOutput;
import others.Observer;
import others.Subject;
import vehicules.Vehicule;
import vehicules.VehiculeFactory;

public class Station implements Subject{	
	public static int NB_DAYS_BEFORE_STEAL = 2;
	public static int POURCENTAGE_CHANCE_VOLE = 10;
	public static int POURCENTAGE_CHANCE_LOCATION = 25;
	
	private final List<Observer> observers = new ArrayList<Observer>();
	private final List<Vehicule> unUsedVehicule = new ArrayList<Vehicule>();
	
	private final UUID id = UUID.randomUUID();
	private int nbDaysEmpty = 0;
	private int nbPlaceInUse = 0;
	private int nbPlaceMax;
	private final Vehicule[] parking;
	
	public Station(int nbPlaceMax, Centre observer, boolean peuplement) {
		this.nbPlaceMax = nbPlaceMax;
		this.parking = new Vehicule[nbPlaceMax];
		this.addObserver(observer);
		
		if(peuplement==true) {
			this.peuplementAleatoire();
		}
	}
	
	public Station(int nbPlaceMax, Centre observer) {
		this(nbPlaceMax, observer, true);
	}
	
	/**
	 * Retourne l'index de la première place de parking vide
	 * Sinon retourne -1
	 * @return int
	 */
	public int whereToPark() {
		if(this.isParkingFull()) return -1;
		
		int idx = 0;
		while(idx<nbPlaceMax && !this.placeIsFree(idx)) {
			idx++;
		}
		
		if(idx>=nbPlaceMax) return -1;
		else return idx;
	}
	
	/**
	 * Retourne true si la place de parking est vide
	 * @param idx (int) = indice de la place à vérifier
	 * @return boolean
	 */
	private boolean placeIsFree(int idx) {
		return idx>=0 && idx<nbPlaceMax && parking[idx]==null;
	}
	
	/**
	 * Stationne un vehicule donner en param sur place de parking donner en param
	 * @param idx (int) = indice de la place sur laquelle on va essayer de stationner le vehicule
	 * @param vehicule (Vehicule) = vehicule que l'on va essayer de stationner
	 * @param use (boolean) = Si True : ça veut dire que le vehicule a était utiliser (tjr à true sauf lors de l'initialisation du parking)
	 */
	public void park(int idx, Vehicule vehicule, boolean use) throws OccupiedPlaceException, IndexOutOfBoundsException {
		if(idx<0 || idx>nbPlaceMax) throw new IndexOutOfBoundsException();
		
		if(!this.placeIsFree(idx)) {
			throw new OccupiedPlaceException();
		}else {
			parking[idx] = vehicule;
			if(parking[idx]!=null) nbPlaceInUse++;
			if(use) parking[idx].use();
		}
	}
	
	/**
	 * Méthode qui appel la fonction "park" en mettant systématiquement le param "use" à "true"
	 * @param idx
	 * @param vehicule
	 * @throws OccupiedPlaceException
	 * @throws IndexOutOfBoundsException
	 */
	public void park(int idx, Vehicule vehicule) throws OccupiedPlaceException, IndexOutOfBoundsException{
		park(idx, vehicule, true);
	}
	
	/**
	 * Retourne le vehicule qui est stationné sur la place (s'il existe et s'il est prêt à partir)
	 * @param idx (int) = indice du vehicule que l'on veut retiré
	 * @return Vehicule
	 */
	public Vehicule unPark(int idx, boolean isSteal) throws IndexOutOfBoundsException, AlreadyFreePlaceException {
		if(idx<0 || idx>nbPlaceMax) throw new IndexOutOfBoundsException();
		
		if(this.placeIsFree(idx)) {
			throw new AlreadyFreePlaceException();
		}else {
			if(this.parking[idx].isReadyToGo() || isSteal) {
				nbPlaceInUse--;
				Vehicule vehicule = parking[idx];
				vehicule.resetNbJourSansReservation(this);
				
				parking[idx] = null;
				return vehicule;
			}else {
				return null;
			}
		}
	}
	
	/**
	 * Méthode qui va appelé la méthode unPark en précisant forcement le paramètre "isSteal" à "false"
	 * @param idx
	 * @return
	 * @throws IndexOutOfBoundsException
	 * @throws AlreadyFreePlaceException
	 */
	public Vehicule unPark(int idx) throws IndexOutOfBoundsException, AlreadyFreePlaceException {
		return this.unPark(idx, false);
	}
	
	/**
	 * Retourne true si le parking est plein
	 * @return boolean
	 */
	public boolean isParkingFull() {
		return nbPlaceInUse>=nbPlaceMax;
	}
	
	/**
	 * Fonction qui peuple aléatoirement le parking de la station
	 */
	public void peuplementAleatoire() {
	    Random rand = new Random();
	    VehiculeFactory factory = new VehiculeFactory();
	    for(int i = 0; i<this.nbPlaceMax; i++) {
		    String type = VehiculeFactory.allVehiculesName.get(rand.nextInt(VehiculeFactory.allVehiculesName.size()));
	    	try {
	    		Vehicule v = factory.createVehicule(type);
	    		if(v!=null) {
	    			v.addObserver(Centre.getInstance().getReparateur());
	    		}
				this.park(i, v, false);
			} catch (OccupiedPlaceException | IndexOutOfBoundsException e) {
				System.out.println("Impossible de stationner le vehicule (idx = "+i+" / type = "+type);
			}
	    }
	}
	
	/**
	 * La méthode permet d'emprunter aléatoirement des vélos sur le parking (un véhicule a 30% de chance d'être emprunter)
	 * @return La liste des vélos qui ont était emprunter
	 */
	public List<Vehicule> unParkRandom(){
		List<Vehicule> res = new ArrayList<Vehicule>();
		Random rand = new Random();
		for(int i = 0; i<this.nbPlaceMax; i++) {
			try {
				if(rand.nextInt(100)<POURCENTAGE_CHANCE_LOCATION) {
					Vehicule v = this.unPark(i);
					if(v!=null)	res.add(v);
				}
			} catch (IndexOutOfBoundsException | AlreadyFreePlaceException e) {}
		}
		return res;
	}
	
	/**
	 * La méthode simule le vole de véhicule parmis une liste de vehicule
	 */
	public void stealRandomVehicules(){
		if(this.unUsedVehicule.size()==0) return;
		
		Random rand = new Random();
		int value = rand.nextInt(100);
		if(value<POURCENTAGE_CHANCE_VOLE) {
			Vehicule v = this.unUsedVehicule.remove(0);
			this.notifyObservers(ColorOutput.RED + "UN \"" + v.getName().toUpperCase() + "\" A ETAIT VOLE" + ColorOutput.RESET);
			unPark(v);
		}
	}
	
	/**
	 * Unpark un vehicule s'il est présent sur le parking
	 * @param v
	 */
	public void unPark(Vehicule v) {
		int i = 0;
		while(i<this.nbPlaceMax && this.parking[i]!=v) {
			i++;
		}
		try {
			this.unPark(i, true);
		} catch (IndexOutOfBoundsException | AlreadyFreePlaceException e) {
			System.out.println("Erreur lors de la tentative de vole");
		}
	}
	
	/**
	 * Incrémente le jour sans reservation de tous les vehicules sur le parking 
	 */
	public void incNbJourOnPark() {
		for(Vehicule v : this.parking) {
			if(v!=null) {
				v.incNbJourSansReservation(this);
			}
		}
	}
	
	@Override
	public String toString() {
		String res = "Station :\n=> ";
		for(Vehicule v : this.parking) {
			if(v==null) res += "[ ]";
			else res += v.toString();
		}
		return res + "\n\n";
	}
	
	public int getnbPlaceMax() {
		return nbPlaceMax;
	}
	
	public void setnbPlaceMax(int nbPlaceMax) {
		this.nbPlaceMax = nbPlaceMax;
	}
	
	public int getNbDaysEmpty() {
		return nbDaysEmpty;
	}
	
	public void setNbDaysEmpty(int nbDaysEmpty) {
		this.nbDaysEmpty = nbDaysEmpty;
	}
	
	public UUID getId() {
		return id;
	}

	public Vehicule[] getParking() {
		return parking;
	}

	public int getNbPlaceInUse() {
		return nbPlaceInUse;
	}

	public void setNbPlaceInUse(int nbPlaceInUse) {
		this.nbPlaceInUse = nbPlaceInUse;
	}
	
	public void sendToReparation() {
		for(Vehicule v : this.parking) {
			if(v!=null && !v.isReadyToGo()) {
				v.getObservers().update(v);
				unPark(v);
			}
		}
	}
	
	public void addVehiculeInListUnused(Vehicule v) {
		this.unUsedVehicule.add(v);
	}
	
	public void removeVehiculeFromListUnused(Vehicule v) {
		this.unUsedVehicule.remove(v);
	}
	
	/**
	 * PATTERN DESIGN : OBSERVER
	 */

	@Override
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyObservers(Object message) {
		for(Observer o : observers) {
			if(o!=null) o.update(message);
		}
	}
}
