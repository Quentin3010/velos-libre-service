package others;

/**
 * Design pattern Observer/Notifier vu en cours
 * @author qbern
 *
 */
public interface Subject {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers(Object message);
}