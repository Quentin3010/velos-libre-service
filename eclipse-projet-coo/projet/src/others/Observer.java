package others;

/**
 * Design pattern Observer/Notifier vu en cours
 * @author qbern
 *
 */
public interface Observer {
    void update(Object message);
}
