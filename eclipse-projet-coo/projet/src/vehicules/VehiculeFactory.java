package vehicules;

import java.util.Arrays;
import java.util.List;

public class VehiculeFactory {
	public static List<String> allVehiculesName = Arrays.asList("VeloElectrique", "VeloClassique", null);
	
	public VehiculeFactory() {
	}
	
	/**
	 * FACTORY : Sert à créer un véhicule selon le design patern de la factory
	 * @param type (String) : type du véhicule voulu
	 * @return
	 */
	public Vehicule createVehicule(String type) {
		if(type==null) return null;
		switch(type) {
		case "VeloElectrique" :
			return new VeloElectrique(false, false);
		case "VeloClassique" :
			return new VeloClassique(false, false);
		default:
			return null;
		}
	}
}
