package vehicules;

public class VeloClassique extends IVelo{

	public VeloClassique(boolean panier, boolean porteBagage) {
		super(panier, porteBagage);
	}

	@Override
	public String getCharacter() {
		return "V";
	}

	@Override
	public String getName() {
		return "Velo Classique";
	}
}
