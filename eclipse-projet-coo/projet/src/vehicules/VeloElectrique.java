package vehicules;

public class VeloElectrique extends IVelo{
	
	public VeloElectrique(boolean panier, boolean porteBagage) {
		super(panier, porteBagage);
	}

	@Override
	public String getCharacter() {
		return "E";
	}

	@Override
	public String getName() {
		return "Velo electrique";
	}
}
