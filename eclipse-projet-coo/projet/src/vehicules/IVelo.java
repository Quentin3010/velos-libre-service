package vehicules;

public abstract class IVelo extends Vehicule{
	public static int NB_MAX_LOCATION_UNTIL_REPAIR = 5;
	
	private boolean panier;
	private boolean porteBagage;

	public IVelo(boolean panier, boolean porteBagage) {
		super();
		this.panier = panier;
		this.porteBagage = porteBagage;
	}
	
	@Override
	public int nbLocationBeforeRepair() {
		return NB_MAX_LOCATION_UNTIL_REPAIR;
	}

	public boolean isPanier() {
		return panier;
	}

	public void setPanier(boolean panier) {
		this.panier = panier;
	}

	public boolean isPorteBagage() {
		return porteBagage;
	}

	public void setPorteBagage(boolean porteBagage) {
		this.porteBagage = porteBagage;
	}
}
