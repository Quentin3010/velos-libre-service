package vehicules;

import others.ColorOutput;
import others.Observer;
import others.Subject;
import station.Station;

public abstract class Vehicule implements Subject {
	private Observer observers; //Un seul réparateur attitré par véhicule
	private int nbReservation; //reset à chaque réparation
	private int nbJourEnReparation = 0;
	private int nbJourSansReservationConsecutive; //reset après chaque réservation
	
	public Vehicule() {
		this.nbReservation = -1;
		this.nbJourSansReservationConsecutive = -1;
	}
	
	/**
	 * Retourne vraie si le vélo est prêt à quitter le parc à vélo
	 * @return
	 */
	public boolean isReadyToGo() {
		return !needRepair();
	}
	
	/**
	 * Retourne le nombre max de location avant de devoir être réparer
	 * @return
	 */
	public abstract int nbLocationBeforeRepair();
	
	/**
	 * Retourne vrai si le vélo à besoin d'être réparé
	 * @return boolean
	 */
	public boolean needRepair() {
		return nbReservation>=nbLocationBeforeRepair();
	}
	
	/**
	 * Fonction qui va changer les variables du véhicules après qu'il ait était utilisé
	 */
	public void use() {
		this.incNbReservation();
	}
	
	/**
	 * Incrémente le nombre de jour sans réservation
	 */
	public void incNbJourSansReservation(Station station) {
		setNbJourSansReservationConsecutive(getNbJourSansReservationConsecutive() + 1);
		if(this.nbJourSansReservationConsecutive==Station.NB_DAYS_BEFORE_STEAL) {
			station.addVehiculeInListUnused(this);
		}
	}
	
	/**
	 * Reset le nombre de jour sans réservation
	 */
	public void resetNbJourSansReservation(Station station) {
		setNbJourSansReservationConsecutive(-1);
		station.removeVehiculeFromListUnused(this);
	}
	
	/**
	 * Retourne le caractère qui symbolise le véhicule
	 * @return
	 */
	public abstract String getCharacter();
	
	/**
	 * Retourne le nom du véhicule
	 * @return
	 */
	public abstract String getName();
	
	public void incNbReservation() {
		this.nbReservation++;
	}

	public int getNbReservation() {
		return nbReservation;
	}

	public void setNbReservation(int nbReservation) {
		this.nbReservation = nbReservation;
	}

	public int getNbJourSansReservationConsecutive() {
		return nbJourSansReservationConsecutive;
	}

	public void setNbJourSansReservationConsecutive(int nbJourSansReservationConsecutive) {
		this.nbJourSansReservationConsecutive = nbJourSansReservationConsecutive;
	}	
	
	public int getNbJourEnReparation() {
		return nbJourEnReparation;
	}

	public void setNbJourEnReparation(int nbJourEnReparation) {
		this.nbJourEnReparation = nbJourEnReparation;
	}
	
	@Override
	public String toString() {
		if(this.needRepair()) return "["+ColorOutput.RED+getCharacter()+ColorOutput.RESET+"]";
		else if(this.nbJourSansReservationConsecutive>=Station.NB_DAYS_BEFORE_STEAL) return "["+ColorOutput.BLUE+getCharacter()+ColorOutput.RESET+"]";
		else return "["+ColorOutput.GREEN+getCharacter()+ColorOutput.RESET+"]";
	}
	
	public Observer getObservers() {
		return observers;
	}

	/**
	 * DESIGN PATTERN : OBSERVER
	 */

	@Override
	public void addObserver(Observer observer) {
		this.observers = observer;
	}

	@Override
	public void removeObserver(Observer observer) {
		this.observers = null;
	}

	@Override
	public void notifyObservers(Object message) {
		this.observers.update(message);
	}
}
