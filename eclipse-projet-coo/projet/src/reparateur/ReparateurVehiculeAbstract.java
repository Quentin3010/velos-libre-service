package reparateur;

import java.util.ArrayList;
import java.util.List;

import centre.Centre;
import others.Observer;
import vehicules.Vehicule;

/**
 * Inspiré du design pattern "Strategy" pour pouvoir avoir plusieurs type de réparateur qui appelerait tous la
 * méthode "doReparation" peut importe le réparateur
 * @author qbern
 *
 */
public abstract class ReparateurVehiculeAbstract implements Observer{
	private final Centre parent;
	private List<Vehicule> vehiculeEnReparation = new ArrayList<Vehicule>();
	
	public ReparateurVehiculeAbstract(Centre centre) {
		this.parent = centre;
	}
	
	@Override
	public void update(Object message) {
		vehiculeEnReparation.add((Vehicule) message);
	}

	/**
	 * Méthode abstract qui est appelé pour faire la réparation des véhicules en cours de réparation
	 * => attributs de classe : "vehiculeEnReparation"
	 */
	public abstract void doReparation();

	public Centre getParent() {
		return parent;
	}

	public List<Vehicule> getVehiculeEnReparation() {
		return vehiculeEnReparation;
	}

	protected void setVehiculeEnReparation(List<Vehicule> vehiculeEnReparation) {
		this.vehiculeEnReparation = vehiculeEnReparation;
	}
}
