package reparateur;

import java.util.ArrayList;
import java.util.List;

import centre.Centre;
import others.ColorOutput;
import vehicules.Vehicule;

public class ReparateurVehciuleGeneral extends ReparateurVehiculeAbstract {
	private static final int NB_JOUR_TO_FULLY_REPAIR = 2;

	public ReparateurVehciuleGeneral(Centre centre) {
		super(centre);
	}

	@Override
	public void doReparation() {
		List<Vehicule> newList = new ArrayList<Vehicule>();
		for(Vehicule v : this.getVehiculeEnReparation()) {
			v.setNbJourEnReparation(v.getNbJourEnReparation()+1);
			if(v.getNbJourEnReparation()==NB_JOUR_TO_FULLY_REPAIR) {
				v.setNbReservation(0);
				v.setNbJourEnReparation(0);
				this.getParent().parkRandom(v);
				System.out.println(ColorOutput.YELLOW + "UN VEHICULE A FINI SA REPARATION !" + ColorOutput.RESET);
			}else {
				newList.add(v);
			}
		}
		this.setVehiculeEnReparation(newList);
	}

}
