package centre;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import exceptions.OccupiedPlaceException;
import others.Observer;
import reparateur.ReparateurVehiculeAbstract;
import reparateur.ReparateurVehciuleGeneral;
import station.Station;
import vehicules.Vehicule;

public class Centre implements Observer {
	public static final int NB_JOUR_VIDE_AVANT_REPEUPLEMENT = 4;
	private RepeuplementAbstract typeRepeuplement;

	public static Centre instance;
	private static ReparateurVehiculeAbstract reparateur;
	private int jour = 1;
	private List<Station> stations = new ArrayList<Station>();
	private List<Vehicule> currentVehiculeLocated = new ArrayList<Vehicule>();
	
	private Centre() {
	}
	
	/**
	 * SINGLETON : Permet de créer/obtenir la seul est unique instance de la classe Centre
	 * @return Centre
	 */
	public static Centre getInstance() {
		if(instance==null) {
			instance = new Centre();
		}
		return instance;
	}
	
	/**
	 * La méthode ajoute une station à liste des stations supervisé par le centre
	 * @param s
	 */
	public void addStation(Station s) {
		this.stations.add(s);
	}
	
	/**
	 * La méthode simule les actions du centre lors d'une journée
	 */
	public void passDay() {
		//La réparation des véhicules progresse tous les matins
		Centre.reparateur.doReparation();
		//On envoit les véhicules qui ont besoin d'être réparer
		for(Station s : stations) {
			s.sendToReparation();
		}
		//On check si une station est vide de NB_JOUR_VIDE_AVANT_REPEUPLEMENT
		//Si c'est le cas, on repeuple TOUTES les stations du centre
		getTypeRepeuplement().repeuplement(instance);

		System.out.println("\n###########\nCENTRE Matin - jour "+ jour +" - nbVehicule : "+ getTotalVehicule() +" \n###########\n");
		System.out.println(this);
		
		//Vehicule qui ont était louer
		System.out.println("###########\nBilan de la journee\n###########\n");
		//La station ouvre et les clients prennent des véhicules
		unParkRandomAll();
		afficheVehiculesEnLocation();
		//Il y a possibilité qu'un véhicule se fasse voler
		for(Station s : stations) {
			s.stealRandomVehicules();
			s.incNbJourOnPark();
		}
		//Les véhicules reviennent de location
		parkRandomAll();
		
		//Etat du centre au soir
		System.out.println("\n\n###########\nCENTRE Soir - jour "+ jour +" - nbVehicule : "+ getTotalVehicule() +" \n###########\n");
		System.out.println(this);
		this.jour++;
	}
	
	/**
	 * Méthode qui affiche les véhicules en location
	 */
	public void afficheVehiculesEnLocation() {
		String res = "Vehicules Loues : ";
		for(Vehicule v : currentVehiculeLocated) {
			res += v.toString();
		}
		System.out.println(res);
	}
	
	/**
	 * La méthode emprunte aléatoirement des véhicules sur toutes les stations qu'elles supervisent
	 * Il y a une petite probabilité que des vélos se fassent voler
	 */
	public void unParkRandomAll(){
		List<Vehicule> res = currentVehiculeLocated;
		for(Station s : this.stations) {
			res.addAll(s.unParkRandom());
		}
		
		currentVehiculeLocated  = res;
	}
	
	/**
	 * La méthode va chercher a garer aléatoirement toutes les vehicule en cours de location 
	 */
	public void parkRandomAll() {
		while(!currentVehiculeLocated.isEmpty()) {
			Vehicule v = this.currentVehiculeLocated.remove(0);
			parkRandom(v);
		}
	}
	
	/**
	 * La méthode va garer aléatoirement le véhicule passer en paramètres dans l'une des stations du centre
	 * @param v
	 */
	public void parkRandom(Vehicule v) {
		Random rand = new Random();
		//On choisit un parking aléatoire non plein
		int numStation = 0;
		int numPlaceFree = -1;
		do{
			numStation = rand.nextInt(this.stations.size());
			numPlaceFree = this.stations.get(numStation).whereToPark();
		}while(this.stations.get(numStation).isParkingFull() || numPlaceFree==-1);
		
		try {
			this.stations.get(numStation).park(numPlaceFree, v);
		} catch (OccupiedPlaceException | IndexOutOfBoundsException e) {
			System.out.println("Impossible de garer le vehicule / station = " + numStation + " / idx = " + numPlaceFree);
		}
	}
	
	/**
	 * Retourne le nombre total de véhicule présent dans toutes les stations du centre
	 * @return
	 */
	public int getTotalVehicule() {
		int i = 0;
		for(Station s : this.stations) {
			for(Vehicule v : s.getParking()) {
				if(v!=null) i++;
			}
		}
		return i;
	}
	
	@Override
	public String toString() {
		String res = "";
		for(Station s : this.stations) {
			res += s.toString();
		}
		res += "En reparation : ";
		for(Vehicule v : Centre.reparateur.getVehiculeEnReparation()) {
			res += v.toString() + ", ";
		}
		return res + "\n";
	}

	public ReparateurVehiculeAbstract getReparateur() {
		return reparateur;
	}

	public void setReparateur(ReparateurVehciuleGeneral reparateur) {
		Centre.reparateur = reparateur;
	}
	
	public List<Station> getStations(){
		return this.stations;
	}
	
	public RepeuplementAbstract getTypeRepeuplement() {
		return typeRepeuplement;
	}

	public void setTypeRepeuplement(RepeuplementAbstract typeRepeuplement) {
		this.typeRepeuplement = typeRepeuplement;
	}

	/**
	 * DESIGN PATTERN : OBSERVER
	 */
	
	@Override
	public void update(Object message) {
		System.out.println((String) message);
	}

	public void setReuplementType(RepeuplementAbstract r) {
		this.setTypeRepeuplement(r);
	}
}
