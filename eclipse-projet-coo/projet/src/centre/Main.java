package centre;

import reparateur.ReparateurVehciuleGeneral;
import station.Station;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		Centre centre = Centre.getInstance();
		centre.setReparateur(new ReparateurVehciuleGeneral(centre));
		centre.setReuplementType(new RepeuplementAleatoire());
		
		//Ajout de la station 1
		centre.addStation(new Station(10, centre));
		//Ajout de la station 2
		centre.addStation(new Station(10, centre));
		//Ajout de la station 3
		centre.addStation(new Station(10, centre));

		//Début de la simulation
		while(true) {
			refreshScreen();

			centre.passDay();
			Thread.sleep(2000);
		}
	}
	
	/**
	 * Méthode qui passe X lignes pour "refresh" l'écran
	 */
	private static void refreshScreen() {
		for(int i = 0; i<100; i++) {
			System.out.println("\n");
		}
	}
}
