package centre;

import others.ColorOutput;

public abstract class RepeuplementAbstract {
    
	/**
	 * Vérifie si une des stations remplie les conditions qui déclenche un repeuplement, 
	 * sinon actualise les stations qui sont vides
	 * @param centre
	 */
	public void repeuplement(Centre centre) {
        int i = 0;
		while(i<centre.getStations().size() && centre.getStations().get(i).getNbDaysEmpty()!=Centre.NB_JOUR_VIDE_AVANT_REPEUPLEMENT){
			if(centre.getStations().get(i).getNbPlaceInUse()==0) {
				centre.getStations().get(i).setNbDaysEmpty(centre.getStations().get(i).getNbDaysEmpty()+1);
			}
			i++;
		}

        if(i<centre.getStations().size()) {
        	doRepeuplement(centre);
        	System.out.println(ColorOutput.YELLOW + "REPEUPLEMENT DES STATIONS SUITE A UNE STATION RESTER TROP LONGTEMPS VIDE" + ColorOutput.RESET);
		}
    }

    /**
     * Effectue le repeuplement en fonction du type de repeuplement
     * @param centre
     */
    public abstract void doRepeuplement(Centre centre);
}
