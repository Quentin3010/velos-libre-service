package centre;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import exceptions.AlreadyFreePlaceException;
import exceptions.OccupiedPlaceException;
import station.Station;
import vehicules.Vehicule;

public class RepeuplementAleatoire extends RepeuplementAbstract{

    @Override
    public void doRepeuplement(Centre centre) {
        List<Vehicule> list = new ArrayList<Vehicule>();
        for(Station s : centre.getStations()){
            for(int i = 0; i<s.getnbPlaceMax(); i++){
                try {
                    list.add(s.unPark(i));
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("erroooooooor");
                    break;
                } catch (AlreadyFreePlaceException e) {}
            }
        }

        Random rand = new Random();
        while(!list.isEmpty()){
            int nbStation = rand.nextInt(centre.getStations().size());
            if(!centre.getStations().get(nbStation).isParkingFull()){
                try {
                    centre.getStations().get(nbStation).park(centre.getStations().get(nbStation).whereToPark(), list.get(0));
                    list.remove(0);
                } catch (OccupiedPlaceException | IndexOutOfBoundsException e) {}
            }
        }
    }
    
}
