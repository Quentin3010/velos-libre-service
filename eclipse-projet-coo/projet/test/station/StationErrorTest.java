package station;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exceptions.AlreadyFreePlaceException;
import exceptions.OccupiedPlaceException;
import vehicules.VehiculeFactory;
import vehicules.VeloElectrique;

public class StationErrorTest {
	private static VehiculeFactory factory;
	private static Station station;
	
	@BeforeEach
	public void beforeEach() {
		factory = new VehiculeFactory(); 
		station = new Station(20, null, false);
	}
	
	@Test
	/**
	 * On test de stationner un vehicule dans la station avec un indice incorrect
	 * avec un indice invalide
	 */
	public void testParkWithIncorrectIndex() {
		VeloElectrique v = (VeloElectrique) factory.createVehicule("VeloElectrique");
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> {
			station.park(-1, v);
		});
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> {
			station.park(station.getnbPlaceMax()+1, v);
		});
	}
	
	@Test
	/**
	 * On test de garer deux fois consécutifs sur la même place un véhicule,
	 * normalement pas de problème la 1ère vois, mais la 2ème fois, une exception est levé
	 * @throws OccupiedPlaceException
	 * @throws IndexOutOfBoundsException
	 */
	public void testParkOnOccupiedPlace() throws OccupiedPlaceException, IndexOutOfBoundsException {
		VeloElectrique v = (VeloElectrique) factory.createVehicule("VeloElectrique");

		station.park(0, v);
		
		Assertions.assertThrows(OccupiedPlaceException.class, () -> {
			station.park(0, v);
		});
	}
	
	@Test
	/**
	 * On test de unPark un vehicule de la station avec un indice incorrect
	 */
	public void testUnparkWithIncorrectIndex() {
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> {
			station.unPark(-1);
		});
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> {
			station.unPark(station.getnbPlaceMax()+1);
		});
	}
	
	@Test
	/**
	 * On test de unPark une place vide
	 */
	public void testUnparkFreePlace() {
		Assertions.assertThrows(AlreadyFreePlaceException.class, () -> {
			station.unPark(0);
		});
	}
}
