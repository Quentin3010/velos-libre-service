package station;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exceptions.AlreadyFreePlaceException;
import exceptions.OccupiedPlaceException;
import vehicules.VehiculeFactory;
import vehicules.VeloElectrique;

public class StationTest {
	private static VehiculeFactory factory;
	private static Station station;
	
	@BeforeEach
	public void beforeEach() {
		factory = new VehiculeFactory();
	}
	
	@Test
	/**
	 * On test d'obtenir l'indice sur lequel on peut garer un vehicule sur un parking vide
	 */
	public void testCanParkOnEmptyStation() {
		station = new Station(20, null, false);
		//La méthode whereToPark() retourne -1 s'il n'y a aucune place de dispo pour se stationner
		assertNotEquals(station.whereToPark(), -1);
		//Ici le parking est vide, donc l'indice retourner doit être 0
		assertEquals(station.whereToPark(), 0);
	}
	
	@Test
	/**
	 * On test de garer un vehicule sur un parking vide
	 * @throws OccupiedPlaceException
	 * @throws IndexOutOfBoundsException
	 */
	public void testParkOnEmptyStation() throws OccupiedPlaceException, IndexOutOfBoundsException {
		station = new Station(20, null, false);
		//On vérifie que la parking soit vide et qu'on a le premier idx libre où garer le vehicule (normalement = 0)
		assertEquals(station.getNbPlaceInUse(), 0);
		assertEquals(station.getParking()[0], null);
		int numeroPlace = station.whereToPark();
		assertEquals(station.whereToPark(), 0);
		
		//On gare un vehicule au premier emplacement de libre (normalement = 0)
		VeloElectrique v = (VeloElectrique) factory.createVehicule("VeloElectrique");
		station.park(numeroPlace, v);
		
		//On vérifie que le vehicule est bien stationner et que le prochaine idx de place vide est 1
		assertEquals(station.getNbPlaceInUse(), 1);
		assertNotEquals(station.getParking()[0], null);
		numeroPlace = station.whereToPark();
		assertEquals(numeroPlace, 1);
	}
	
	@Test
	/**
	 * On test d'obtenir l'indice sur lequel on peut garer un vehicule sur un parking plein
	 * @throws OccupiedPlaceException
	 */
	public void testCantParkFullStation() throws OccupiedPlaceException {
		station = new Station(20, null, false);
		VeloElectrique v = (VeloElectrique) factory.createVehicule("VeloElectrique");
		
		//On gare 20 véhicules sur la station de 20 places
		for(int i = 0; i<station.getnbPlaceMax(); i++) {
			station.park(station.whereToPark(), v);
		}
		
		//Le parking est plein, donc normalement on obtient -1 quand on souhaite obtenir l'indice de la place
		assertEquals(station.whereToPark(), -1);
	}
	
	@Test
	/**
	 * On test de garer puis de unPark un vehicule
	 * @throws OccupiedPlaceException
	 * @throws IndexOutOfBoundsException
	 * @throws AlreadyFreePlaceException
	 */
	public void testUnpark() throws OccupiedPlaceException, IndexOutOfBoundsException, AlreadyFreePlaceException {
		station = new Station(20, null, false);
		//On récupère l'indice de la place
		int numeroPlace = station.whereToPark();
		assertEquals(station.getParking()[numeroPlace], null);
		
		//On gare le vehicule
		VeloElectrique v = (VeloElectrique) factory.createVehicule("VeloElectrique");
		station.park(numeroPlace, v);
		assertNotEquals(station.getParking()[numeroPlace], null);
		
		//On enlève le véhicule
		station.unPark(numeroPlace);
		assertEquals(station.getParking()[numeroPlace], null);
	}
	
	@Test
	/**
	 * On test le vole d'un véhicule sur un parking (en mettant le taux de vol à 100%)
	 */
	public void testStealVehicule() {
		station = new Station(20, null, true);
		Station.POURCENTAGE_CHANCE_VOLE = 100;
	
		int nbVehiculeInitiale = station.getNbPlaceInUse();
		
		//Un véhicule ne peut être volé qu'après ne pas avoir était louer "NB_DAYS_BEFORE_STEAL" jours
		//On essaye de procéder au vol avant "NB_DAYS_BEFORE_STEAL" jours (pas possible normalement)
		station.stealRandomVehicules();
		assertEquals(station.getNbPlaceInUse(), nbVehiculeInitiale);
		
		//On passe le nombre de jour d'inactivité du véhicule et on tente de le voler (succès normalement)
		for(int i = 0; i<Station.NB_DAYS_BEFORE_STEAL+1; i++)
			station.incNbJourOnPark();
		station.stealRandomVehicules();
		assertNotEquals(station.getNbPlaceInUse(), nbVehiculeInitiale);
	}
	
}
