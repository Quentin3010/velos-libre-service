package centre;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import reparateur.ReparateurVehciuleGeneral;
import station.Station;

public class CentreRepeuplementTest {
	private static Centre centre;

	@BeforeEach
	public void beforeEach() {
		Centre.instance = null;
		centre = Centre.getInstance();
        centre.setReparateur(new ReparateurVehciuleGeneral(centre));
        centre.setReuplementType(new RepeuplementAleatoire());
		//Ajout de la station 1
		centre.addStation(new Station(20, centre));
		//Ajout de la station 2
		centre.addStation(new Station(20, centre, false));
	}

    @Test
    /**
     * On test le bon fonctionnement du repeuplement aléatoire de station quand une station (ici la station 2)
     * reste vide "NB_JOUR_VIDE_AVANT_REPEUPLEMENT" jours consécutifs
     */
    public void testRepeuplementAleatoire(){
        //On vérifie que la station 1 est bien remplie (pas forcement entièrement) et la 2 vide
    	assertNotEquals(centre.getStations().get(0).getNbPlaceInUse(), 0);
        assertEquals(centre.getStations().get(1).getNbPlaceInUse(), 0);

        //On appel "NB_JOUR_VIDE_AVANT_REPEUPLEMENT" fois la méthode de repeuplement
        for(int i = 0; i<Centre.NB_JOUR_VIDE_AVANT_REPEUPLEMENT; i++)
        	centre.getTypeRepeuplement().repeuplement(centre);

        //Normalement, pas de changement sur le parking
        assertNotEquals(centre.getStations().get(0).getNbPlaceInUse(), 0);
        assertEquals(centre.getStations().get(1).getNbPlaceInUse(), 0);

        //Maintenant au "NB_JOUR_VIDE_AVANT_REPEUPLEMENT"+1 jour, il y a un repeuplement aléatoire car 
        //la station 2 est resté ""NB_JOUR_VIDE_AVANT_REPEUPLEMENT" jour vide
        centre.getTypeRepeuplement().repeuplement(centre);
        assertNotEquals(centre.getStations().get(0).getNbPlaceInUse(), 0);
        assertNotEquals(centre.getStations().get(1).getNbPlaceInUse(), 0);
    }
}