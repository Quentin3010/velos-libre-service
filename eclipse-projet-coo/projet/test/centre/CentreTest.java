package centre;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import station.Station;

public class CentreTest {
	private static Centre centre;

	@BeforeEach
	public void beforeEach() {
		Centre.instance = null;
		centre = Centre.getInstance();
		//Ajout de la station 1
		centre.addStation(new Station(20, centre));
		//Ajout de la station 2
		centre.addStation(new Station(20, centre));
		//Ajout de la station 3
		centre.addStation(new Station(20, centre));	
	}

	@Test
	/**
	 * Test qui vérifie que tous les vehicule qui sont partis en location via la méthode "unParkRandomAll"
	 * revienne grâce à la méthode "parkRandomAll"
	 */
	public void testAllVehiculeComeBackAfterLocation() {
		int nbVehiculeDeb = centre.getTotalVehicule();
		centre.unParkRandomAll();
		
		centre.parkRandomAll();
		int nbVehiculeFIn = centre.getTotalVehicule();
		
		assertEquals(nbVehiculeDeb, nbVehiculeFIn);
	}
	
	@Test
	/**
	 * Test qui vérifie que tous les vehicule qui sont partis en location via la méthode "unParkRandomAll"
	 * revienne grâce à la méthode "parkRandomAll" et que si on réutilise la méthode "parkRandomAll", ils ne
	 * sont pas remis en doublon 
	 */
	public void testNumberTotalDontChangeIfParkRandomAllTwoTimes() {
		int nbVehiculeDeb = centre.getTotalVehicule();
		centre.unParkRandomAll();
		centre.parkRandomAll();
		int nbVehiculeFIn = centre.getTotalVehicule();
		
		assertEquals(nbVehiculeDeb, nbVehiculeFIn);
		
		centre.parkRandomAll();
		nbVehiculeFIn = centre.getTotalVehicule();
		
		assertEquals(nbVehiculeDeb, nbVehiculeFIn);
	}
}
