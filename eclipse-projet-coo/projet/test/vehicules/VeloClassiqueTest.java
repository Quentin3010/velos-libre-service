package vehicules;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class VeloClassiqueTest {
	private static VehiculeFactory factory;
	
	@BeforeEach
	void beforeEach() {
		factory = new VehiculeFactory();
	}
	
	/**
	 * Test que l'initialisation via la factory est correcte
	 */
	@Test
	void testVehiculeFactory() {
		assertTrue(factory.createVehicule("VeloElectrique") instanceof VeloElectrique);
	}
	
	/**
	 * Test pour vérifier que le vehicule est prêt pour partir dès ça création
	 */
	@Test
	void testReadyToGoWhenInit() {
		VeloElectrique v = (VeloElectrique) factory.createVehicule("VeloElectrique");
		assertTrue(v.isReadyToGo());
	}
	
	@Test 
	/**
	 * On test la méthode needRepair
	 */
	void testNeedRepair(){
		VeloElectrique v = (VeloElectrique) factory.createVehicule("VeloElectrique");
		v.setNbReservation(IVelo.NB_MAX_LOCATION_UNTIL_REPAIR);
		assertTrue(v.needRepair());
	}
}
