package vehicules;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FactoryErrorTest {
	private static VehiculeFactory factory;
	
	@BeforeEach
	void beforeEach() {
		factory = new VehiculeFactory();
	}
	
	@Test
	/**
	 * On test de donner un argument invalide dans la factory
	 */
	void testIllegalArgumentException() {
		assertEquals(factory.createVehicule("error"), null);
	}
}
