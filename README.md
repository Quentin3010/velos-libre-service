# Vélo Libre Service

Quentin BERNARD

## Description du projet

Le projet "Vélo Libre Service" a été développé dans le cadre de mon projet de fin de semestre de S5 en Licence Informatique, avec un accent particulier sur la mise en application de divers design patterns tels que le singleton, le factory, le strategy, et l'observer. L'objectif principal était de concevoir une simulation d'un système de location de vélos en libre-service, similaire aux systèmes bien connus tels que Vélib ou V’Lille.

La simulation reproduit un écosystème de vélos en libre-service, avec des stations de location équipées de vélos classiques et à assistance électrique. Chaque station est identifiée par un identifiant unique et a une capacité d'accueil variable (de 10 à 20 emplacements). Les utilisateurs peuvent déposer un vélo dans un emplacement vide ou retirer un vélo d'un emplacement occupé. Un centre de contrôle supervise l'ensemble du système, gérant les dépôts, les locations, et orchestrant la redistribution des vélos entre les stations lorsque nécessaire.

Le projet a été réalisé en Java, en utilisant des principes de programmation orientée objet (POO) et en mettant en œuvre divers design patterns pour assurer une structure flexible et évolutive.

## Fonctionnalités et Mise en oeuvre

- Simulation de stations de location :
- Capacités variables, équipées de vélos classiques et à assistance électrique.
- Possibilité d'ajouter d'autres types de vélos facilement.
- Gestion des vélos :
- Dépôt et retrait de vélos selon les règles définies.
- Suivi des dépôts, des locations, et redistribution des vélos par le centre de contrôle.
- Intégration des réparations :
- Mise hors-service des vélos après un certain nombre de locations.
- Envoi d'un réparateur pour effectuer des contrôles et réparations pendant un intervalle de temps.
- Prévention du vol :
- Vérification du statut de chaque vélo pour détecter les cas potentiels de vol.
- Exclusion des vélos volés du système de location.
- Gestion temporelle :
- Réalisation d'un nombre aléatoire de dépôts et de retraits de vélos à intervalles définis.
- Redistribution des vélos entre les stations si une station reste vide ou pleine trop longtemps.
- Extension à d'autres modes de déplacement :
- Conception flexible permettant l'ajout d'autres modes de déplacements urbains, tels que les trottinettes électriques, avec des spécifications similaires.
- Interaction avec d'autres corps de métier :
- Possibilité d'intégrer d'autres corps de métier, comme des changements de peinture de la flotte, pour des événements ponctuels tels que les JO.

## Mon rôle

J'ai réalisé intégralement ce projet seul, en respectant les spécifications du sujet. Mon implication a couvert tous les aspects du développement, de la conception du modèle à la mise en œuvre des fonctionnalités, en mettant particulièrement l'accent sur l'application des design patterns mentionnés.



