# PROJET COO - Vélos Libre Service

BERNARD Quentin (Groupe 2 - L3 Informatique)

## HOW TO

**IMPORTANT : LS PROCEDURES ONT ETAIT TESTE SUR UN ENVIRONNEMENT LINUX**

Récupérer les sources du projet depuis votre dépôt
```
Cloner mon projet sur votre machine (accessible en étant connecté au réseau de l'univ ou avec VPN de l'univ) :
=> git clone https://gitlab-etu.fil.univ-lille.fr/quentin.bernard4.etu/coo-bernard-quentin.git

Une fois cloner, ce mettre à la racine du projet (. étant la racine du projet), puis : 
=> cd ./eclipse-projet-coo/projet

Vous voila dans le répertoire où se trouve tous mes fichiers source et test
```

Générer la documentation (précisez la localisation de cette documentation);
```
Ce mettre à la racine du projet (. étant la racine du projet), puis : 
=> cd ./eclipse-projet-coo/projet/src
=> javadoc -d ../docs -Xdoclint:none **/*.java

La documentation est maintenant généré et se trouve : "./eclipse-projet-coo/projet/docs"
```

Compiler et exécuter les sources. N’oubliez pas de préciser où se placer pour exécuter ces commandes;
```
Ce mettre à la racine du projet (. étant la racine du projet), puis :  
=> cd ./eclipse-projet-coo/projet
=> make run

La simulation se lance !
```

Compiler et exécuter les tests (voir plus bas);
```
Ce mettre à la racine du projet (. étant la racine du projet), puis :  
=> cd ./eclipse-projet-coo/projet
=> make test

Les tests se lance !
```

Générer et exécuter l’archive (.jar) du projet;
```
Ce mettre à la racine du projet (. étant la racine du projet), puis :  
=> cd ./eclipse-projet-coo/projet
=> javac -d bin src/**/*.java
=> jar cfe MonProjet.jar centre.Main -C bin .

Le jar est maintenant créer (il se trouve donc dans ./eclipse-projet-coo/projet), vous pouvez l'éxecuter avec :
=> java -jar MonProjet.jar
```


## PRESENTATION

### Fonctionnement

Tout d'abord, je vais vous expliquer comment fonctionne la simulation. Un centre est initialisé avec un *Réparateur* et un *Type de reuplement*. Ensuite, on associe des *Stations* au centre et c'est stations sont initialisé avec un nombre de place et des *Véhicules* généré et répartie aléatoirement dans chacun des parkings.

Maintenant que tout est prêt la simulation se lance. Une journée ce découpe en plusieurs étapes : 
- On effectue en partie de la réparation des véhicules qui sont en réparations (si leur réparation est fini suite à ça, ils sont retournés dans une station).
- Si de nouveau véhicules doivent être envoyé en réparations on les envoient 
- On vérifie si des stations sont vides (et on incrémente un compteur de jour consécutif). Si une station est resté vide trop longtemps, on procède à un repeuplement de toutes les stations.
- **Les stations ouvrent, et on affiche dans le terminal l'état des station à l'ouverture (et le véhicules qui sont en réparation).**
- Les véhicules peuvent maintenant être loué
- On affiche les véhicules qui ont était loué.
- Un véhicules resté trop longtemps sur un parking peut être voler (si un vole a lieu, on envoie une alerte dans la console).
- Les véhicules reviennent dans une des stations avant la fin de journée.
- **Les stations ferment, et on affiche dans le terminal l'état des stations à la fermeture (et le véhicules qui sont en réparation).**

Voici une capture d'écran de la simulation (1 centre qui possède 3 stations) :

<img src="./images/interface-simulation.png" alt="Interface" height="300">

*J'ai choisis cette capture spécifique, car elle réunit efficacement tous les éléments cité précédemment.*

### Points techniques

Pour les points techniques que j'aimerai abordé, il y a :

**Les vehicules** : J'ai créer une classe abstract "Vehicule" qui peut être hérité et créer facilement de nouveau véhicules qui pourront être utiliser/stationner dans les stations du centre. De plus, afin de faciliter la création d'instance de Véhicule, j'ai utiliser une Factory.

**Le reuplement** : Lorsqu'une station reste vide trop longtemps, elle déclenche le repeuplement de toutes les stations du centre. Afin de faciliter l'implémentation de différentes méthodes de repeuplement, j'ai utiliser le design pattern "Strategy". De ce fait, il suffira de créer une nouvelle classe qui héritera de "RepeuplementAbstract" et de modifier l'attribut "typeRepeuplement" dans Centre afin d'utiliser une nouvelle méthode de repeuplement. *J'ai utiliser le même principe avec le réparateur de véhicule*.


## TECHNOLOGIES UTILISER

Pour ce projet, j'ai exclusivement utilisé **Java 16** et **JUnit 5** pour les tests. De plus, j'ai créé un fichier **Makefile** pour faciliter la compilation, le lancement du programme, et l'exécution des tests.

En ce qui concerne le code en lui-même, j'ai appliqué plusieurs **patrons de conception** vus en cours :

- **Le Singleton** : J'ai mis en place une instance unique de la classe "Centre" pour gérer le seul centre qui supervise toutes les stations.

- **La Factory** : J'ai utilisé le patron de conception Factory pour générer efficacement les différents types de véhicules.

- **Le Strategy** : J'ai implémenté le patron Strategy afin de permettre l'utilisation de différentes méthodes de tri (pour le repeuplement des stations) et de réparation des véhicules. Cela facilite l'ajout de nouvelles méthodes de tri ou de réparation à l'avenir.

- **L'Observer** : Les stations sont observées par le centre, permettant ainsi à ces dernières de le notifier, par exemple.


## UML

![UML](./UML.png)